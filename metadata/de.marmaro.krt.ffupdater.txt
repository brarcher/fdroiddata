Categories:Internet
License:GPLv3+
Web Site:
Source Code:https://github.com/krt16s/ffupdater
Issue Tracker:https://github.com/krt16s/ffupdater/issues

Auto Name:FFUpdater
Summary:Download and update Firefox
Description:
Simple Updater for Firefox, that guesses the next release of Firefox, downloads
and installs it. If no current Firefox installation is found, it defaults to the
last known release. Note that "next version" only checks for the next major
version, not point releases. If you haven't updated in a long time, you might
have to run the updater several times to be up-to-date.

This app is alpha quality.
.

Repo Type:git
Repo:https://github.com/krt16s/ffupdater

Build:39.0,1
    commit=39.0
    subdir=ffupdater
    gradle=yes

Build:39.0,2
    commit=e0121a69988aa67993c1bf0f099eac4535b4818b
    subdir=ffupdater
    gradle=yes

Build:40.0-test,3
    commit=9678242e49ac810974a5fd69aa46227eca3b7952
    subdir=ffupdater
    gradle=yes
    forceversion=yes

Build:40.0,4
    commit=40.0
    subdir=ffupdater
    gradle=yes
    forceversion=yes

Build:40.0.3,5
    commit=40.0.3
    subdir=ffupdater
    gradle=yes
    forceversion=yes

Build:41.0,6
    commit=41.0
    subdir=ffupdater
    gradle=yes

Build:42.0,7
    commit=42.0
    subdir=ffupdater
    gradle=yes

Build:42.0.1,8
    commit=42.0.1
    subdir=ffupdater
    gradle=yes

Build:43.0,10
    commit=43.0
    subdir=ffupdater
    gradle=yes

Archive Policy:2 versions
Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:43.0
Current Version Code:10
