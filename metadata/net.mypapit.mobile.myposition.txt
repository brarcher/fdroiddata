Categories:Navigation
License:GPLv2+
Web Site:https://github.com/gjedeer/mylocation/blob/HEAD/README.md
Source Code:https://github.com/gjedeer/mylocation
Issue Tracker:https://github.com/gjedeer/mylocation/issues

Auto Name:MYPosition
Summary:Share your location
Description:
Share your location, easily and lightly. Supports plain coordinates, geo:-URI,
OpenStreetMap- and GoogleMaps-Links.
.

Repo Type:git
Repo:https://github.com/gjedeer/mylocation

Build:1.2.1,8
    commit=1928c284570bdee12ce3a4b7f44a7118e20a30b4
    target=android-23

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2.1
Current Version Code:8
