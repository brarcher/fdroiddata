Categories:Theming
License:Apache2
Web Site:https://github.com/ultrafunk/netinfo/blob/HEAD/README.md
Source Code:https://github.com/ultrafunk/netinfo
Issue Tracker:https://github.com/ultrafunk/netinfo/issues

Auto Name:NetInfo Widget
Summary:Widget for network connectivity
Description:
Easy to use single or dual widget with a clean design that shows the current
mobile data and/or WiFi connection details, with quick shortcuts to change the
settings for each of them and easy access to turn any of them ON or OFF.
.

Repo Type:git
Repo:https://github.com/ultrafunk/netinfo

Build:1.5,11
    commit=da899a089e19ae483045b9bfa7c18fb09706a72b
    subdir=NetInfo_Widget
    gradle=yes

Build:1.5.1,12
    commit=2eaa496f69c39ca422afe422fa1abfad90141937
    subdir=NetInfo_Widget
    gradle=yes

Build:1.5.2,13
    commit=2ddcd8a0aed2d712f3df50d9bd4345106bb14d8a
    subdir=NetInfo_Widget
    gradle=yes

Build:1.5.3,14
    commit=547ec2bb6909ac323f87d1c58c7a25cde90a1897
    subdir=NetInfo_Widget
    gradle=yes

Build:1.5.4,15
    commit=4163f308b12ae1cb3a1e990ea26acc3c0d4ed6a2
    subdir=NetInfo_Widget
    gradle=yes

Build:1.5.5,16
    commit=5b35fcce3cf38e7caa3539876273a035d005fcb8
    subdir=NetInfo_Widget
    gradle=yes

Build:1.5.6,17
    commit=baffa2bd4ebe899886ab22f50
    subdir=NetInfo_Widget
    gradle=yes

Build:1.7.0,18
    commit=83dd3795b1f05959fd3c64c770352e0a4bf0220e
    subdir=NetInfo_Widget
    gradle=yes

Build:1.7.1,19
    commit=77dc0884ffae87e0284aa82b4e593b5d7aa6f316
    subdir=NetInfo_Widget
    gradle=yes

Build:1.7.2,20
    commit=d0799c0f67e0deb8e7e073b4cfecff5c961ec4b3
    subdir=NetInfo_Widget
    gradle=yes

Build:1.7.3,21
    commit=ac699ea34bf49dfacfc625b09642bb435572b8db
    subdir=NetInfo_Widget
    gradle=yes

Build:1.7.4,23
    commit=5023c1b5375cefbe98c16962aadeac8a087c27a6
    subdir=NetInfo_Widget
    gradle=yes

Build:1.7.5,24
    commit=4c6268eca7c2e80bf77d176771437850adff6da4
    subdir=NetInfo_Widget
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.7.5
Current Version Code:24
